﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Persona> personas = new List<Persona>();

            int opcion = 1;

            while (opcion != 4)
            {


                Console.WriteLine("1 - Ingresar persona.");
                Console.WriteLine("2 - Mostra personas.");
                Console.WriteLine("3 - Buscar Persona.");
                Console.WriteLine("4 - Salir. \n");

                Console.WriteLine("Opción:");
                opcion = int.Parse(Console.ReadLine());

                if (opcion == 1)
                {
                    Persona p = new Persona();

                    Console.WriteLine("Ingresar RUT: ");
                    p.Rut = Console.ReadLine();

                    Console.WriteLine("Ingresar nombre: ");
                    p.Nombre = Console.ReadLine();

                    Console.WriteLine("Ingresar apellido: ");
                    p.Apellido = Console.ReadLine();

                    Console.WriteLine("Ingresar edad: ");
                    p.Edad = int.Parse(Console.ReadLine());

                    List<int> NotaList = new List<int>();

                    Console.WriteLine("Nota 1 :");
                    NotaList.Add(int.Parse(Console.ReadLine()));


                    Console.WriteLine("Nota 2:");
                    NotaList.Add(int.Parse(Console.ReadLine()));


                    Console.WriteLine("Nota 3:");
                    NotaList.Add(int.Parse(Console.ReadLine()));

                    p.Nota = NotaList;

                    personas.Add(p);
                }

                if (opcion == 2)
                {
                    foreach (Persona x in personas)
                    {
                        Console.WriteLine("Nombre :" + x.NombreCompleto());
                        Console.WriteLine("\n");
                    }
                }

                if (opcion == 3)
                {
                    Console.WriteLine("Buscar por RUT: \n");
                    string rutBuscar = Console.ReadLine();

                    Persona p = new Persona();

                    p = personas.Where(x => x.Rut.Equals(rutBuscar)).FirstOrDefault();

                    if (p == null)
                    {
                        Console.WriteLine("No se han encontrado los datos.");
                    }
                    else
                    {
                        Console.WriteLine("Nombre:" + p.NombreCompleto());
                        Console.WriteLine("Edad:" + p.edadPersona());
                        Console.WriteLine("Promedio de notas:" + p.Promedio());
                        Console.WriteLine("\n");
                    }
                }

                if (opcion == 4)
                {
                    Console.ReadKey();
                }

            }


        }
    }
}
