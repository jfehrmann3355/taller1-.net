﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class
        Persona
    {
        public int MyProperty { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }

        public List<int> Nota { get; set; }

        int suma = 0;
        int promedio = 0;


        public string NombreCompleto()
        {
            return Nombre + " " + Apellido;
        }

        public string RutPersona()
        {
            return Rut;
        }

        public int Promedio()
        {

            for (int i = 0; i < 3; i++)
            {
                suma = Nota[i] + suma;
            }
            promedio = suma / 3;
            return promedio;
        }

        public int edadPersona()
        {
            return Edad;
        }
    }
}